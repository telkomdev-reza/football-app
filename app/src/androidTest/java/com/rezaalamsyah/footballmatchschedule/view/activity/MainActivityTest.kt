package com.rezaalamsyah.footballmatchschedule.view.fragment

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.*
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.rezaalamsyah.footballmatchschedule.R.id.*
import com.rezaalamsyah.footballmatchschedule.view.activity.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.widget.EditText
import android.support.test.espresso.action.ViewActions.pressImeActionButton
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.Espresso.onView



@RunWith(AndroidJUnit4::class)
class MainActivityTest{
    @Rule
    @JvmField var activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testAppBehaviour(){
        onView(withId(botnavigation))
                .check(matches(isDisplayed()))
        onView(withId(navigation_prev))
                .check(matches(isDisplayed()))
        onView(withId(navigation_prev))
                .perform(click())
        onView(withId(tabsMatches))
                .check(matches(isDisplayed()))
        onView(withId(tabsMatches)).perform(click())
        Thread.sleep(2000)
        onView(withId(viewPagerMatches))
                .check(matches(isDisplayed()))
        onView(withId(appBarNext))
                .check(matches(isDisplayed()))
        onView(withId(appBarNext))
                .perform(swipeUp())
        onView(withId(nestedNext))
                .check(matches(isDisplayed()))
        onView(withId(nestedNext))
                .perform(swipeUp())
        Thread.sleep(3000)
        onView(withId(lvNext))
                .check(matches(isDisplayed()))
        onView(withId(rvNextMatch))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(rvNextMatch)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(12, click()))
        Thread.sleep(2000)
        onView(withId(action_favorite))
                .check(matches(isDisplayed()))
        onView(withId(action_favorite))
                .perform(click())
        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
        onView(withId(viewPagerMatches))
                .perform(swipeLeft())
        Thread.sleep(2000)

        onView(withId(nestedPrev))
                .check(matches(isDisplayed()))
        onView(withId(nestedPrev))
                .perform(swipeUp())
        Thread.sleep(3000)
        onView(withId(lvPrev))
                .check(matches(isDisplayed()))
        onView(withId(rvLastMatch))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(rvLastMatch)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(12, click()))
        Thread.sleep(2000)
        onView(withId(action_favorite))
                .check(matches(isDisplayed()))
        onView(withId(action_favorite))
                .perform(click())
        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
        onView(withId(nestedPrev))
                .perform(swipeDown())
        Thread.sleep(1000)
        onView(withId(searchViewMatches))
                .check(matches(isDisplayed()))
        onView(withId(searchViewMatches))
                .perform(click())
        onView(isAssignableFrom(EditText::class.java)).
                perform(typeText("Chelsea"), pressImeActionButton())
        Thread.sleep(4000)
        Espresso.pressBack()
        Thread.sleep(2000)

        onView(withId(botnavigation))
                .check(matches(isDisplayed()))
        onView(withId(navigation_next))
                .check(matches(isDisplayed()))
                .perform(click())
        onView(withId(nestedTeam))
                .check(matches(isDisplayed()))
        onView(withId(lvTeam))
                .check(matches(isDisplayed()))
        onView(withId(rvTeam))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(rvTeam)).
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>
                (0, click()))
        Thread.sleep(2000)
        onView(withId(action_favorite))
                .check(matches(isDisplayed()))
        onView(withId(action_favorite))
                .perform(click())
        Thread.sleep(2000)
        onView(withId(tabsTeams))
                .check(matches(isDisplayed()))
                .perform(click())
        onView(withText("Player"))
                .check(matches(isDisplayed()))
                .perform(click())
        Thread.sleep(2000)
        onView(withId(nestedPlayer))
                .check(matches(isDisplayed()))
        onView(withId(lvPlayer))
                .check(matches(isDisplayed()))
        onView(withId(rvPlayer))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(rvPlayer)).
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>
                (2, click()))
        Thread.sleep(3000)
        Espresso.pressBack()
        Thread.sleep(1000)
        Espresso.pressBack()
        Thread.sleep(2000)
        onView(withId(appBarTeams))
                .check(matches(isDisplayed()))
        onView(withId(toolbarTeams))
                .check(matches(isDisplayed()))
        onView(withId(searchViewTeams))
                .check(matches(isDisplayed()))
        onView(withId(searchViewTeams))
                .perform(click())
        onView(isAssignableFrom(EditText::class.java)).
                perform(typeText("Chelsea"))
        Thread.sleep(2000)
        onView(withId(lvTeam))
                .check(matches(isDisplayed()))
        onView(withId(rvTeam))
                .check(matches(isDisplayed()))
        onView(withId(rvTeam)).
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>
                (0, click()))
        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)

        onView(withId(botnavigation))
                .check(matches(isDisplayed()))
        onView(withId(navigation_fav))
                .check(matches(isDisplayed()))
                .perform(click())
        onView(withId(tabsFav))
                .check(matches(isDisplayed()))
        onView(withId(tabsFav)).perform(click())
        onView(withId(viewPagerFav))
                .check(matches(isDisplayed()))
        onView(withId(nestedFav))
                .check(matches(isDisplayed()))
        onView(withId(nestedFav))
                .perform(swipeUp())
        Thread.sleep(1000)
        onView(withId(lvFav))
                .check(matches(isDisplayed()))
        onView(withId(rvFavMatch))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(rvFavMatch)).
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>
                (0, click()))
        Thread.sleep(2000)
        onView(withId(action_favorite))
                .check(matches(isDisplayed()))
        onView(withId(action_favorite))
                .perform(click())
        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
        onView(withId(nestedFav))
                .perform(swipeDown())
        Thread.sleep(2000)
        onView(withId(nestedFav))
                .perform(swipeLeft())
        Thread.sleep(2000)
        onView(withId(nestedFavteam))
                .check(matches(isDisplayed()))
        onView(withId(nestedFavteam))
                .perform(swipeUp())
        Thread.sleep(1000)
        onView(withId(lvFavteam))
                .check(matches(isDisplayed()))
        onView(withId(rvFavteam))
                .check(matches(isDisplayed()))
        Thread.sleep(2000)
        onView(withId(rvFavteam)).
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>
                (0, click()))
        Thread.sleep(2000)
        onView(withId(action_favorite))
                .check(matches(isDisplayed()))
        onView(withId(action_favorite))
                .perform(click())
        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
        onView(withId(nestedFavteam))
                .perform(swipeDown())
        Thread.sleep(2000)

    }

}