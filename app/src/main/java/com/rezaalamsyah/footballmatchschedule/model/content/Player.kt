package com.rezaalamsyah.footballmatchschedule.model.content

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Player : Serializable {

    @SerializedName("strNationality")
    @Expose
    var strNationality: String? = null
    @SerializedName("strPlayer")
    @Expose
    var strPlayer: String? = null
    @SerializedName("strTeam")
    @Expose
    var strTeam: String? = null
    @SerializedName("strDescriptionEN")
    @Expose
    var strDescriptionEN: String? = null
    @SerializedName("strPosition")
    @Expose
    var strPosition: String? = null
    @SerializedName("strFacebook")
    @Expose
    var strFacebook: String? = null
    @SerializedName("strTwitter")
    @Expose
    var strTwitter: String? = null
    @SerializedName("strInstagram")
    @Expose
    var strInstagram: String? = null
    @SerializedName("strHeight")
    @Expose
    var strHeight: String? = null
    @SerializedName("strWeight")
    @Expose
    var strWeight: String? = null
    @SerializedName("strCutout")
    @Expose
    var strCutout: String? = null
    @SerializedName("strFanart1")
    @Expose
    var strFanart1: String? = null

}