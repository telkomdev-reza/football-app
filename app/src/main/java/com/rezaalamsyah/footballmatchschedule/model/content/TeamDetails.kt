package com.rezaalamsyah.footballmatchschedule.model.content

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TeamDetails : Serializable {

        @SerializedName("idTeam")
        @Expose
        var idTeam: String? = null
        @SerializedName("strTeam")
        @Expose
        var strTeam: String? = null
        @SerializedName("intFormedYear")
        @Expose
        var intFormedYear: String? = null
        @SerializedName("strStadium")
        @Expose
        var strStadium: String? = null
        @SerializedName("strDescriptionEN")
        @Expose
        var strDescriptionEN: String? = null
        @SerializedName("strTeamBadge")
        @Expose
        var strTeamBadge: String? = null
}