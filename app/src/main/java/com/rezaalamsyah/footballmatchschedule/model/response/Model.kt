package com.rezaalamsyah.footballmatchschedule.model.response

import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.model.content.Player
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails

object Model {
    data class Events (
            val events: List<LastMatch>
    )

    data class Search (
            val event: List<LastMatch>
    )

    data class TeamDetail (
            val teams : List<TeamDetails>
    )

    data class Players (
            val player : List<Player>
    )
}