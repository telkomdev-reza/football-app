package com.rezaalamsyah.footballmatchschedule.presenter.connection.service

import java.net.URL

class ApiRepo{
    fun doRequest(url: String): String {
        return URL(url).readText()
    }
}