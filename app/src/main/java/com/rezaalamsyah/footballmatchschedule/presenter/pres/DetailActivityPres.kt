package com.rezaalamsyah.footballmatchschedule.presenter.pres

import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.model.response.Model
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiConfigTSDB
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.view.DetailActivityView
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.coroutines.experimental.bg

class DetailActivityPres (private val view: DetailActivityView,
                          private val apiRepository: ApiRepo,
                          private val gson: Gson,
                          private val context: CoroutineContextProvider){

    fun getTeamDetails(strHomeTeam: String,strAwayTeam: String){
        view.showLoading()
        launch(context.main) {
            val data = bg{
                gson.fromJson(apiRepository
                        .doRequest(ApiConfigTSDB.getTeamDetail(strHomeTeam)),
                        Model.TeamDetail::class.java
                )
            }
            view.showHomeTeamDetails(data.await().teams)
        }
        launch(context.main) {
            val data = bg{
                gson.fromJson(apiRepository
                        .doRequest(ApiConfigTSDB.getTeamDetail(strAwayTeam)),
                        Model.TeamDetail::class.java
                )
            }
            view.showAwayTeamDetails(data.await().teams)
        }
        view.hideLoading()
    }
}