package com.rezaalamsyah.footballmatchschedule.presenter.pres

import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.model.response.Model
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiConfigTSDB
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.view.ActivityMainView
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.coroutines.experimental.bg

class NextMatchFragmentPres(private val view: ActivityMainView,
                            private val apiRepository: ApiRepo,
                            private val gson: Gson,
                            private val context: CoroutineContextProvider){

    fun getNextMatch(leagueId : String){
        view.showLoading()
        launch(context.main) {
            val data = bg{
                gson.fromJson(apiRepository
                        .doRequest(ApiConfigTSDB.getNextMatch(leagueId)),
                        Model.Events::class.java
                )
            }
            if (data.await().events!=null)
                view.showMatchList(data.await().events)
            else
                view.nullData()
            view.hideLoading()
        }
    }
}
