package com.rezaalamsyah.footballmatchschedule.presenter.pres

import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.model.response.Model
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiConfigTSDB
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.view.ActivityMainView
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.coroutines.experimental.bg

class SearchMatchPres(private val view: ActivityMainView,
                            private val apiRepository: ApiRepo,
                            private val gson: Gson,
                            private val context: CoroutineContextProvider){

    fun getSearchMatch(teamName : String){
        view.showLoading()
        launch(context.main) {
            val data = bg{
                gson.fromJson(apiRepository
                        .doRequest(ApiConfigTSDB.getSearchMatch(teamName)),
                        Model.Search::class.java
                )
            }
            if (data.await().event!=null)
                view.showMatchList(data.await().event)
            else
                view.nullData()

            view.hideLoading()
        }
    }

}