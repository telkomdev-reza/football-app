package com.rezaalamsyah.footballmatchschedule.presenter.pres

import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.model.response.Model
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiConfigTSDB
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.view.ActivityMainView
import com.rezaalamsyah.footballmatchschedule.presenter.view.TeamFragmentView
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.coroutines.experimental.bg

class TeamFragmentPres(private val view: TeamFragmentView,
                       private val apiRepository: ApiRepo,
                       private val gson: Gson,
                       private val context: CoroutineContextProvider){

    fun getListTeam(leagueId : String){
        view.showLoading()
        launch(context.main) {
            val data = bg{
                gson.fromJson(apiRepository
                        .doRequest(ApiConfigTSDB.getListTeam(leagueId)),
                        Model.TeamDetail::class.java
                )
            }
            if (data.await().teams!=null)
                view.showTeamList(data.await().teams)
            else
                view.nullData()
            view.hideLoading()
        }
    }

    fun getSearchTeam(leagueId : String){
        launch(context.main) {
            val data = bg{
                gson.fromJson(apiRepository
                        .doRequest(ApiConfigTSDB.getTeamDetail(leagueId)),
                        Model.TeamDetail::class.java
                )
            }
            if (data.await().teams!=null)
                view.showTeamList(data.await().teams)
            else
                view.nullData()
        }
    }
}