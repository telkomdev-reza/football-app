package com.rezaalamsyah.footballmatchschedule.presenter.pres

import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.model.response.Model
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiConfigTSDB
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.view.TeamFragmentView
import com.rezaalamsyah.footballmatchschedule.presenter.view.TeamPlayerView
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.coroutines.experimental.bg

class TeamPlayerPres(private val view: TeamPlayerView,
                       private val apiRepository: ApiRepo,
                       private val gson: Gson,
                       private val context: CoroutineContextProvider){

    fun getListPlayer(leagueId : String){
        view.showLoading()
        launch(context.main) {
            val data = bg{
                gson.fromJson(apiRepository
                        .doRequest(ApiConfigTSDB.getListPlayer(leagueId)),
                        Model.Players::class.java
                )
            }
            if (data.await().player!=null){
                view.showTeamPlayer(data.await().player)
            }
            else
                view.nullData()
            view.hideLoading()
        }
    }

}