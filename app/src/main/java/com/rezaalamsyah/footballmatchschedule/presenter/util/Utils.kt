package com.rezaalamsyah.footballmatchschedule.presenter.util

import android.annotation.SuppressLint
import android.view.View
import java.text.SimpleDateFormat
import java.util.*

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.GONE
}

@SuppressLint("SimpleDateFormat")
fun dayConversion(strDay: String?): String {
    var strDayIndo = ""
    val dayMonthYear: String
    if (strDay!=null){
        val date = SimpleDateFormat("d/M/yy").parse(strDay)
        val matchDate= SimpleDateFormat("EEEE", Locale.ENGLISH).format(date)
        when (matchDate) {
            "Sunday" -> strDayIndo= "Min"
            "Monday" -> strDayIndo= "Sen"
            "Tuesday" -> strDayIndo= "Sel"
            "Wednesday" -> strDayIndo= "Rab"
            "Thursday" -> strDayIndo= "Kam"
            "Friday" -> strDayIndo= "Jum"
            "Saturday" -> strDayIndo= "Sab"
        }
        dayMonthYear= SimpleDateFormat(", d MMM 20yy", Locale.ENGLISH).format(date)
    }else{
        dayMonthYear="To be confirmed soon"
    }
    val fulllDate = strDayIndo+dayMonthYear
    return fulllDate
}

fun convertTime(str: String?): String{
    val newTime : String
    if (str==null)
        newTime = "-"
    else newTime = if (str != "")
        str.substring(0,5)
    else
        "-"
    return newTime
}

fun replaceString(str : String?) : String{
    val newString : String
    if (str==null)
        newString = "-"
    else newString = if (str != "")
        str.replace("; ", "\n")
    else
        "-"
    return newString
}

fun replaceStrColNospace(str : String?) : String{
    val newString : String
    if (str==null)
        newString = "-"
    else newString = if (str != "")
        str.replace(";", "\n")
    else
        "-"
    return newString
}

fun getIdLeague(name: String) : String{
    var idLeague = ""
    when(name){
        "English Premier League" -> idLeague="4328"
        "German Bundesliga" -> idLeague="4399"
        "Spanish La Liga" -> idLeague="4335"
        "Italian Serie A" -> idLeague="4332"
        "French Ligue 1" -> idLeague="4334"
    }
    return idLeague
}

