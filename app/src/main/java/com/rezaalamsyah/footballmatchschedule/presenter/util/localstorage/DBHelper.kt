package com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class DBHelper(ctx:Context): ManagedSQLiteOpenHelper(ctx,"Favorite.db",null,1){

    companion object {
        private var instance: DBHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DBHelper{
            if (instance == null) {
                instance = DBHelper(ctx.applicationContext)
            }
            return instance as DBHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(LocalModel.Favorite.TABLE_FAV, true,
                LocalModel.Favorite.ID_FAV to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                LocalModel.Favorite.ID_EVENT to TEXT + UNIQUE,
                LocalModel.Favorite.NAME_EVENT to TEXT,
                LocalModel.Favorite.HOME_TEAM_EVENT to TEXT,
                LocalModel.Favorite.AWAY_TEAM_EVENT to TEXT,
                LocalModel.Favorite.HOME_SCORE_EVENT to TEXT,
                LocalModel.Favorite.AWAY_SCORE_EVENT to TEXT,
                LocalModel.Favorite.HOME_GOALD_EVENT to TEXT,
                LocalModel.Favorite.HOME_RCARD_EVENT to TEXT,
                LocalModel.Favorite.HOME_YCARD_EVENT to TEXT,
                LocalModel.Favorite.HOME_GK_EVENT to TEXT,
                LocalModel.Favorite.HOME_DEF_EVENT to TEXT,
                LocalModel.Favorite.HOME_MIDF_EVENT to TEXT,
                LocalModel.Favorite.HOME_FWD_EVENT to TEXT,
                LocalModel.Favorite.HOME_FORMATION_EVENT to TEXT,
                LocalModel.Favorite.AWAY_GOALD_EVENT to TEXT,
                LocalModel.Favorite.AWAY_RCARD_EVENT to TEXT,
                LocalModel.Favorite.AWAY_YCARD_EVENT to TEXT,
                LocalModel.Favorite.AWAY_GK_EVENT to TEXT,
                LocalModel.Favorite.AWAY_DEF_EVENT to TEXT,
                LocalModel.Favorite.AWAY_MIDF_EVENT to TEXT,
                LocalModel.Favorite.AWAY_FWD_EVENT to TEXT,
                LocalModel.Favorite.AWAY_FORMATION_EVENT to TEXT,
                LocalModel.Favorite.HOME_SHOTS_EVENT to TEXT,
                LocalModel.Favorite.AWAY_SHOTS_EVENT to TEXT,
                LocalModel.Favorite.DATE_EVENT to TEXT)

        db.createTable(LocalModel.FavTeam.TABLE_FAVTEAM, true,
                LocalModel.FavTeam.ID_FAV to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                LocalModel.FavTeam.ID_TEAM to TEXT + UNIQUE,
                LocalModel.FavTeam.BADGE to TEXT,
                LocalModel.FavTeam.NAME_TEAM to TEXT,
                LocalModel.FavTeam.FORMED_YEAR to TEXT,
                LocalModel.FavTeam.STADIUM to TEXT,
                LocalModel.FavTeam.TEAM_DESC to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(LocalModel.Favorite.TABLE_FAV, true)
        db.dropTable(LocalModel.FavTeam.TABLE_FAVTEAM, true)
    }
}

val Context.database: DBHelper
    get() = DBHelper.getInstance(applicationContext)