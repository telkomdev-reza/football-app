package com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage

import java.io.Serializable

object LocalModel {
    data class Favorite(val id:Long?, val teamId : String?,val eventName: String?,val homeTeam: String?,val awayTeam: String?,val homeScore: String?,
                        val awayScore: String?,val homeGoald: String?,val homeRcard: String?,val homeYcard: String?,val homeGk: String?,val homeDef: String?,
                        val homeMid: String?,val homeFwd: String?,val homeForm: String?,val awayGoald: String?,val awayRcard: String?,val awayYcard: String?,
                        val awayGK: String?,val awayDef: String?,val awayMid: String?,val awayFwd: String?,val awayForm: String?,val homeShot: String?,
                        val awayShot : String?,val dateEvent : String?) : Serializable{
        companion object {
            const val TABLE_FAV: String = "TABLE_FAVORITE"
            const val ID_FAV : String = "ID_"
            const val ID_EVENT :String = "EVENT_ID"
            const val NAME_EVENT :String = "EVENT_NAME"
            const val HOME_TEAM_EVENT :String = "EVENT_HOME_TEAM"
            const val AWAY_TEAM_EVENT :String = "EVENT_AWAY_TEAM"
            const val HOME_SCORE_EVENT :String = "EVENT_HOME_SCORE"
            const val AWAY_SCORE_EVENT :String = "EVENT_AWAY_SCORE"
            const val HOME_GOALD_EVENT :String = "EVENT_HOME_GOAL_DETAIL"
            const val HOME_RCARD_EVENT :String = "EVENT_HOME_RED_CARD"
            const val HOME_YCARD_EVENT :String = "EVENT_HOME_YELLOW_CARD"
            const val HOME_GK_EVENT :String = "EVENT_HOME_GK"
            const val HOME_DEF_EVENT :String = "EVENT_HOME_DEF"
            const val HOME_MIDF_EVENT :String = "EVENT_HOME_MIDF"
            const val HOME_FWD_EVENT :String = "EVENT_HOME_FWD"
            const val HOME_FORMATION_EVENT :String = "EVENT_HOME_FORMATION"
            const val AWAY_GOALD_EVENT :String = "EVENT_AWAY_GOAL_DETAIL"
            const val AWAY_RCARD_EVENT :String = "EVENT_AWAY_RED_CARD"
            const val AWAY_YCARD_EVENT :String = "EVENT_AWAY_YELLOW_CARD"
            const val AWAY_GK_EVENT :String = "EVENT_AWAY_GK"
            const val AWAY_DEF_EVENT :String = "EVENT_AWAY_DEF"
            const val AWAY_MIDF_EVENT :String = "EVENT_AWAY_MIDF"
            const val AWAY_FWD_EVENT :String = "EVENT_AWAY_FWD"
            const val AWAY_FORMATION_EVENT :String = "EVENT_AWAY_FORMATION"
            const val HOME_SHOTS_EVENT :String = "EVENT_HOME_SHOTS"
            const val AWAY_SHOTS_EVENT :String = "EVENT_AWAY_SHOTS"
            const val DATE_EVENT :String = "EVENT_DATE"
        }
    }

    data class FavTeam(val id:Long?, val teamId:String?, val strBadge:String?,val strTeam:String?,val strYear:String?,val strStadium:String?,
                       val strDesc:String?): Serializable{
        companion object {
            const val TABLE_FAVTEAM: String = "TABLE_FAVTEAM"
            const val ID_FAV : String = "ID_"
            const val ID_TEAM:String = "TEAM_ID"
            const val BADGE:String = "TEAM_BADGE"
            const val NAME_TEAM:String = "TEAM_NAME"
            const val FORMED_YEAR:String = "TEAM_FORMED_YEAR"
            const val STADIUM:String = "TEAM_STADIUM"
            const val TEAM_DESC:String = "TEAM_DESC"
        }
    }
}