package com.rezaalamsyah.footballmatchschedule.presenter.view

import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch

interface ActivityMainView {
    fun showLoading()
    fun hideLoading()
    fun showMatchList(dataLastMatch: List<LastMatch>)
    fun nullData()
}