package com.rezaalamsyah.footballmatchschedule.presenter.view

import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails

interface DetailActivityView {
    fun showLoading()
    fun hideLoading()
    fun showHomeTeamDetails(dataTeamDetails: List<TeamDetails>)
    fun showAwayTeamDetails(dataTeamDetails: List<TeamDetails>)
}