package com.rezaalamsyah.footballmatchschedule.presenter.view

import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails

interface TeamFragmentView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(dataListTeam : List<TeamDetails>)
    fun nullData()
}