package com.rezaalamsyah.footballmatchschedule.presenter.view

import com.rezaalamsyah.footballmatchschedule.model.content.Player
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails

interface TeamPlayerView {
    fun showLoading()
    fun hideLoading()
    fun showTeamPlayer(dataPlayer: List<Player>)
    fun nullData()
}