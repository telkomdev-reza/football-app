package com.rezaalamsyah.footballmatchschedule.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import com.rezaalamsyah.footballmatchschedule.R
import android.support.v4.app.Fragment;
import android.view.MenuItem
import com.rezaalamsyah.footballmatchschedule.R.id.*
import com.rezaalamsyah.footballmatchschedule.view.fragment.menu.FavoriteFragment
import com.rezaalamsyah.footballmatchschedule.view.fragment.menu.MatchesFragment
import com.rezaalamsyah.footballmatchschedule.view.fragment.menu.TeamsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeFragment()

    }

    private fun initializeFragment(){
        loadFragment(MatchesFragment())
        val nav : BottomNavigationView = botnavigation
        nav.setOnNavigationItemSelectedListener(this)
    }

    private fun loadFragment(fragment: Fragment?) : Boolean{
        if (fragment != null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit()
            return true
        }
        return false
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            navigation_prev -> loadFragment(MatchesFragment())
            navigation_next -> loadFragment(TeamsFragment())
            navigation_fav  -> loadFragment(FavoriteFragment())
        }
        return true
    }

}
