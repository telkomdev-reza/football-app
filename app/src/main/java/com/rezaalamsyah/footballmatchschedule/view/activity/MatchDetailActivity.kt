package com.rezaalamsyah.footballmatchschedule.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import com.rezaalamsyah.footballmatchschedule.R.drawable.*
import com.rezaalamsyah.footballmatchschedule.R.menu.*
import com.rezaalamsyah.footballmatchschedule.R.id.action_favorite
import com.rezaalamsyah.footballmatchschedule.R.layout.activity_match_detail
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails
import com.rezaalamsyah.footballmatchschedule.presenter.pres.DetailActivityPres
import com.rezaalamsyah.footballmatchschedule.presenter.util.invisible
import com.rezaalamsyah.footballmatchschedule.presenter.util.visible
import com.rezaalamsyah.footballmatchschedule.presenter.view.DetailActivityView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_match_detail.*
import org.jetbrains.anko.toast
import android.database.sqlite.SQLiteConstraintException
import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.LocalModel
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.database
import com.rezaalamsyah.footballmatchschedule.view.viewholder.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class MatchDetailActivity : AppCompatActivity(), DetailActivityView {

    private lateinit var presenter : DetailActivityPres
    private lateinit var idEvent: String
    private lateinit var dataMatch : LastMatch
    private lateinit var dataFav : LocalModel.Favorite
    private var homeTeam = ""
    private var awayTeam = ""
    private var from = ""
    private var menuItem : Menu? = null
    private var isFav : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_match_detail)
        initUI()
        getDataFromIntent()
        favState()
        initRequest()
    }

    private fun initRequest() {
        val request = ApiRepo()
        val gson = Gson()
        val contextCoroutine = CoroutineContextProvider()
        presenter = DetailActivityPres(this,request,gson,contextCoroutine)
        presenter.getTeamDetails(homeTeam,awayTeam)
    }

    private fun initUI(){
        setSupportActionBar(toolbar_detailprev)
        toolbar_detailprev.setNavigationIcon(ic_back)
        toolbar_detailprev.setNavigationOnClickListener{finish()}
        Picasso.get().load(ic_wallpaper_stadium).noFade().into(img_wp_detailprev)
    }

    override fun showLoading() {
        lvProgressBarDetail.visible()
    }

    override fun hideLoading() {
        lvProgressBarDetail.invisible()
    }

    override fun showHomeTeamDetails(dataTeamDetails: List<TeamDetails>) {
        vwHomeTeamDetails(dataTeamDetails,this)
    }

    override fun showAwayTeamDetails(dataTeamDetails: List<TeamDetails>) {
        vwAwayTeamDetails(dataTeamDetails,this)
    }

    private fun getDataFromIntent() {
        val intent = intent

        from = intent.getStringExtra("key_from")
        if (from.equals("match")){
            dataMatch = intent.getSerializableExtra("key_data") as LastMatch
            vwMatchDetails(dataMatch,this)
        }else if (from.equals("favorite")){
            dataFav = intent.getSerializableExtra("key_data") as LocalModel.Favorite
            vwFavMatchDetails(dataFav,this)
        }

        homeTeam = intent.getStringExtra("key_homeTeam")
        awayTeam = intent.getStringExtra("key_awayTeam")
        idEvent =  intent.getStringExtra("key_idEvent")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            action_favorite -> {
                if (isFav) removeFromFav()
                else addToFavorite()
                isFav = !isFav
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(fav,menu)
        menuItem = menu
        setFavorite()
        return true
    }

    private fun addToFavorite(){
        try {
            database.use {
                if (from.equals("match")){
                    insert(LocalModel.Favorite.TABLE_FAV,
                            LocalModel.Favorite.ID_EVENT to dataMatch.idEvent,
                            LocalModel.Favorite.NAME_EVENT to dataMatch.strEvent,
                            LocalModel.Favorite.HOME_TEAM_EVENT to dataMatch.strHomeTeam,
                            LocalModel.Favorite.AWAY_TEAM_EVENT to dataMatch.strAwayTeam,
                            LocalModel.Favorite.HOME_SCORE_EVENT to dataMatch.intHomeScore,
                            LocalModel.Favorite.AWAY_SCORE_EVENT to dataMatch.intAwayScore,
                            LocalModel.Favorite.HOME_GOALD_EVENT to dataMatch.strHomeGoalDetails,
                            LocalModel.Favorite.HOME_RCARD_EVENT to dataMatch.strHomeRedCards,
                            LocalModel.Favorite.HOME_YCARD_EVENT to dataMatch.strHomeYellowCards,
                            LocalModel.Favorite.HOME_GK_EVENT to dataMatch.strHomeLineupGoalkeeper,
                            LocalModel.Favorite.HOME_DEF_EVENT to dataMatch.strHomeLineupDefense,
                            LocalModel.Favorite.HOME_MIDF_EVENT to dataMatch.strHomeLineupMidfield,
                            LocalModel.Favorite.HOME_FWD_EVENT to dataMatch.strHomeLineupForward,
                            LocalModel.Favorite.HOME_FORMATION_EVENT to dataMatch.strHomeFormation,
                            LocalModel.Favorite.AWAY_GOALD_EVENT to dataMatch.strAwayGoalDetails,
                            LocalModel.Favorite.AWAY_RCARD_EVENT to dataMatch.strAwayRedCards,
                            LocalModel.Favorite.AWAY_YCARD_EVENT to dataMatch.strAwayYellowCards,
                            LocalModel.Favorite.AWAY_GK_EVENT to dataMatch.strAwayLineupGoalkeeper,
                            LocalModel.Favorite.AWAY_DEF_EVENT to dataMatch.strAwayLineupDefense,
                            LocalModel.Favorite.AWAY_FWD_EVENT to dataMatch.strAwayLineupForward,
                            LocalModel.Favorite.AWAY_FORMATION_EVENT to dataMatch.strAwayFormation,
                            LocalModel.Favorite.HOME_SHOTS_EVENT to dataMatch.intHomeShots,
                            LocalModel.Favorite.AWAY_SHOTS_EVENT to dataMatch.intAwayShots,
                            LocalModel.Favorite.DATE_EVENT to dataMatch.strDate)
                }else{
                    insert(LocalModel.Favorite.TABLE_FAV,
                            LocalModel.Favorite.ID_EVENT to dataFav.id,
                            LocalModel.Favorite.NAME_EVENT to dataFav.eventName,
                            LocalModel.Favorite.HOME_TEAM_EVENT to dataFav.homeTeam,
                            LocalModel.Favorite.AWAY_TEAM_EVENT to dataFav.awayTeam,
                            LocalModel.Favorite.HOME_SCORE_EVENT to dataFav.homeScore,
                            LocalModel.Favorite.AWAY_SCORE_EVENT to dataFav.awayScore,
                            LocalModel.Favorite.HOME_GOALD_EVENT to dataFav.homeGoald,
                            LocalModel.Favorite.HOME_RCARD_EVENT to dataFav.homeRcard,
                            LocalModel.Favorite.HOME_YCARD_EVENT to dataFav.awayYcard,
                            LocalModel.Favorite.HOME_GK_EVENT to dataFav.homeGk,
                            LocalModel.Favorite.HOME_DEF_EVENT to dataFav.homeDef,
                            LocalModel.Favorite.HOME_MIDF_EVENT to dataFav.homeMid,
                            LocalModel.Favorite.HOME_FWD_EVENT to dataFav.homeFwd,
                            LocalModel.Favorite.HOME_FORMATION_EVENT to dataFav.homeForm,
                            LocalModel.Favorite.AWAY_GOALD_EVENT to dataFav.awayGoald,
                            LocalModel.Favorite.AWAY_RCARD_EVENT to dataFav.awayRcard,
                            LocalModel.Favorite.AWAY_YCARD_EVENT to dataFav.awayYcard,
                            LocalModel.Favorite.AWAY_GK_EVENT to dataFav.homeGk,
                            LocalModel.Favorite.AWAY_DEF_EVENT to dataFav.awayDef,
                            LocalModel.Favorite.AWAY_FWD_EVENT to dataFav.homeFwd,
                            LocalModel.Favorite.AWAY_FORMATION_EVENT to dataFav.awayForm,
                            LocalModel.Favorite.HOME_SHOTS_EVENT to dataFav.homeShot,
                            LocalModel.Favorite.AWAY_SHOTS_EVENT to dataFav.awayShot,
                            LocalModel.Favorite.DATE_EVENT to dataFav.dateEvent)
                }
            }
            toast("ADDED TO FAVORITE")
        }catch (e: SQLiteConstraintException){
        }
    }

    private fun removeFromFav(){
        try {
            database.use {
                delete(LocalModel.Favorite.TABLE_FAV, "(EVENT_ID = {id})",
                            "id" to idEvent)
            }
            toast("REMOVED FROM FAVORITE")
        } catch (e: SQLiteConstraintException){

        }
    }

    private fun favState(){
        database.use {
            val result = select(LocalModel.Favorite.TABLE_FAV)
                    .whereArgs("(EVENT_ID = {id})",
                            "id" to idEvent)
            val favorite = result.parseList(classParser<LocalModel.Favorite>())
            if (!favorite.isEmpty())
                isFav = true
        }
    }

    private fun setFavorite() {
        if (isFav)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_fav)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_fav)
    }
}
