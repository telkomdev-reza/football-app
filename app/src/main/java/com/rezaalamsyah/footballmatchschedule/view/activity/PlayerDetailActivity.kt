package com.rezaalamsyah.footballmatchschedule.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.R.layout.activity_player_detail
import com.rezaalamsyah.footballmatchschedule.model.content.Player
import com.rezaalamsyah.footballmatchschedule.view.viewholder.vwPlayerDetail
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_player_detail.*

class PlayerDetailActivity : AppCompatActivity() {
    private lateinit var dataPlayer : Player

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_player_detail)
        getDataFromIntent()
        initUI()
    }

    private fun initUI(){
        setSupportActionBar(toolbar_detailplayer)
        toolbar_detailplayer.setNavigationIcon(R.drawable.ic_back)
        toolbar_detailplayer.setNavigationOnClickListener{finish()}
    }

    private fun getDataFromIntent() {
        val intent = intent
        dataPlayer = intent.getSerializableExtra("key_data") as Player
        Picasso.get().load(dataPlayer.strFanart1).placeholder(R.drawable.ic_wallpaper_stadium).into(imgWpplayer)
        vwPlayerDetail(dataPlayer,this)
    }
}
