package com.rezaalamsyah.footballmatchschedule.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.google.gson.Gson
import com.lapism.searchview.Search
import com.rezaalamsyah.footballmatchschedule.R.layout.activity_search
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.pres.SearchMatchPres
import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.util.invisible
import com.rezaalamsyah.footballmatchschedule.presenter.util.visible
import com.rezaalamsyah.footballmatchschedule.presenter.view.ActivityMainView
import com.rezaalamsyah.footballmatchschedule.view.adapter.AdapterListSearchMatch
import kotlinx.android.synthetic.main.activity_search.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.onRefresh

class SearchActivity : AppCompatActivity(), ActivityMainView {

    private lateinit var presenter : SearchMatchPres
    private lateinit var adapter : AdapterListSearchMatch
    private var query: String =""
    private var match: MutableList<LastMatch> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_search)
        initRequest()
        searchBar.setText(query)
        intentAction()
        initRecyView()
        searchAction()
        refreshAction()
    }

    private fun initRecyView(){
        rvSMatch.layoutManager = LinearLayoutManager(this)
        rvSMatch.adapter = adapter
    }

    private fun initRequest(){
        val intent = intent
        query = intent.getStringExtra("key_query")
        val request = ApiRepo()
        val gson = Gson()
        val contextCoroutine = CoroutineContextProvider()
        presenter = SearchMatchPres(this,request,gson,contextCoroutine)
        presenter.getSearchMatch(query)
    }

    private fun intentAction(){
        adapter = AdapterListSearchMatch(this,match){
            startActivity<MatchDetailActivity>(
                    "key_data" to it,
                    "key_idEvent" to it.idEvent,
                    "key_homeTeam" to it.strHomeTeam,
                    "key_awayTeam" to it.strAwayTeam,
                    "key_from" to "match"
            )
        }
    }

    private fun searchAction(){
        searchBar.setOnQueryTextListener(object : Search.OnQueryTextListener {
            override fun onQueryTextSubmit(query: CharSequence): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: CharSequence) {
                if(newText.length - 0 == 0){
                    match.clear()
                    adapter.notifyDataSetChanged()
                }else{
                    presenter.getSearchMatch(newText.toString())
                }
            }
        })
    }

    override fun showLoading() {
        swipeRefreshSMatch.isRefreshing = false
        lvProgressBarSMatch.visible()
    }

    override fun hideLoading() {
        lvProgressBarSMatch.invisible()
    }

    override fun showMatchList(dataLastMatch: List<LastMatch>) {
        match.clear()
        match.addAll(dataLastMatch)
        adapter.notifyDataSetChanged()
    }

    override fun nullData() {
        match.clear()
        adapter.notifyDataSetChanged()
    }

    private fun refreshAction() {
        swipeRefreshSMatch.onRefresh {
            presenter.getSearchMatch(query)
        }
    }
}
