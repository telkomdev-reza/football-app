package com.rezaalamsyah.footballmatchschedule.view.activity

import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.R.layout.activity_team_detail
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.LocalModel
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.database
import com.rezaalamsyah.footballmatchschedule.view.adapter.PagerAdapter
import com.rezaalamsyah.footballmatchschedule.view.fragment.tab.TeamOverviewFragment
import com.rezaalamsyah.footballmatchschedule.view.fragment.tab.TeamPlayerFragment
import com.rezaalamsyah.footballmatchschedule.view.viewholder.vwFavTeamDetail
import com.rezaalamsyah.footballmatchschedule.view.viewholder.vwTeamDetail
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_team_detail.*
import org.jetbrains.anko.db.*
import org.jetbrains.anko.toast

class TeamDetailActivity : AppCompatActivity() {
    private var isFav : Boolean = false
    private var menuItem : Menu? = null
    private var from = ""
    private var idTeam = ""
    private var dataTeam : TeamDetails? = null
    private var dataFav: LocalModel.FavTeam? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_team_detail)
        initUI()
        getDataFromIntent()
        favState()
        initPager()
    }
    private fun initUI(){
        setSupportActionBar(toolbar_detailteam)
        toolbar_detailteam.setNavigationIcon(R.drawable.ic_back)
        toolbar_detailteam.setNavigationOnClickListener{finish()}
        Picasso.get().load(R.drawable.ic_wteam).noFade().into(imgWpTeamDetail)
    }

    private fun initPager(){
        val pageAdapter = PagerAdapter(supportFragmentManager)
        pageAdapter.add(TeamOverviewFragment.newInstance(dataTeam,dataFav),"Overview")
        pageAdapter.add(TeamPlayerFragment.newInstance(idTeam),"Player")
        viewPagerTeam.adapter = pageAdapter
        tabsTeams.setupWithViewPager(viewPagerTeam)
    }

    private fun getDataFromIntent() {
        val intent = intent
        from = intent.getStringExtra("key_from")
        if (from.equals("team")){
            dataTeam = intent.getSerializableExtra("key_data") as TeamDetails
            vwTeamDetail(dataTeam,this)
        }else if (from.equals("favorite")){
            dataFav = intent.getSerializableExtra("key_data") as LocalModel.FavTeam
            vwFavTeamDetail(dataFav,this)
        }
        idTeam = intent.getStringExtra("key_idTeam")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_favorite -> {
                if (isFav) removeFromFav()
                else addToFavorite()
                isFav = !isFav
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.fav,menu)
        menuItem = menu
        setFavorite()
        return true
    }

    private fun addToFavorite(){
        try {
            database.use {
                if (from.equals("team")){
                    insert(LocalModel.FavTeam.TABLE_FAVTEAM,
                            LocalModel.FavTeam.ID_TEAM to dataTeam!!.idTeam,
                            LocalModel.FavTeam.BADGE to dataTeam!!.strTeamBadge,
                            LocalModel.FavTeam.NAME_TEAM to dataTeam!!.strTeam,
                            LocalModel.FavTeam.FORMED_YEAR to dataTeam!!.intFormedYear,
                            LocalModel.FavTeam.STADIUM to dataTeam!!.strStadium,
                            LocalModel.FavTeam.TEAM_DESC to dataTeam!!.strDescriptionEN)
                }else{
                    insert(LocalModel.FavTeam.TABLE_FAVTEAM,
                            LocalModel.FavTeam.ID_TEAM to dataFav!!.teamId,
                            LocalModel.FavTeam.BADGE to dataFav!!.strBadge,
                            LocalModel.FavTeam.NAME_TEAM to dataFav!!.strTeam,
                            LocalModel.FavTeam.FORMED_YEAR to dataFav!!.strYear,
                            LocalModel.FavTeam.STADIUM to dataFav!!.strStadium,
                            LocalModel.FavTeam.TEAM_DESC to dataFav!!.strDesc)
                }
            }
            toast("ADDED TO FAVORITE")
        }catch (e: SQLiteConstraintException){
        }
    }

    private fun removeFromFav(){
        try {
            database.use {
                delete(LocalModel.FavTeam.TABLE_FAVTEAM, "(TEAM_ID = {id})",
                        "id" to idTeam)
            }
            toast("REMOVED FROM FAVORITE")
        } catch (e: SQLiteConstraintException){

        }
    }

    private fun favState(){
        database.use {
            val result = select(LocalModel.FavTeam.TABLE_FAVTEAM)
                    .whereArgs("(TEAM_ID = {id})",
                            "id" to idTeam)
            val favorite = result.parseList(classParser<LocalModel.FavTeam>())
            if (!favorite.isEmpty())
                isFav = true
        }
    }

    private fun setFavorite() {
        if (isFav)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_fav)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_fav)
    }
}
