package com.rezaalamsyah.footballmatchschedule.view.adapter

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.app.FragmentActivity
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rezaalamsyah.footballmatchschedule.R.layout.adapter_list_next_match
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.presenter.util.convertTime
import com.rezaalamsyah.footballmatchschedule.presenter.util.dayConversion
import kotlinx.android.synthetic.main.adapter_list_next_match.view.*
import org.jetbrains.anko.textColor

class AdapteListNextMatch(private val context: FragmentActivity?,
                          private val ctx : Context,
                          private val lastMatch: List<LastMatch>,
                          private val listener: (LastMatch) -> Unit):
        RecyclerView.Adapter<AdapteListNextMatch.MatchViewHolder>(){
    private var isReminded = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            MatchViewHolder(LayoutInflater.from(context).inflate(adapter_list_next_match, parent, false))


    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(lastMatch[position],listener,ctx,isReminded)
    }

    override fun getItemCount(): Int {
        return lastMatch.size
    }

    val reminderReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            isReminded = intent.getStringExtra("key_icon")
        }
    }

    class MatchViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        fun bindItem(items: LastMatch, listener: (LastMatch) -> Unit, context: Context, isReminded: String) {

            itemView.txt_next_home_team.text = items.strHomeTeam
            itemView.txt_next_away_team.text= items.strAwayTeam
            val matchDateIndo = dayConversion(items.strDate)
            itemView.txt_next_datematch.text = matchDateIndo
            val time = convertTime(items.strTime)
            itemView.txt_next_time.text = time
            if (items.strDate==null||items.strDate==""){
                itemView.txt_next_datematch.textColor = Color.parseColor("#ad0000")
                itemView.btnReminder.visibility = View.GONE
            }
            itemView.lvClickListMatch.setOnClickListener {
                listener(items)
            }

            itemView.btnReminder.setOnClickListener{
                if (items.strDate!=null||items.strDate==""){
                    val intent = Intent("reminder")
                            .putExtra("key_reminder_date", items.strDate)
                            .putExtra("key_reminder_title", items.strEvent)
                            .putExtra("key_reminder_desc", items.strFilename)
                            .putExtra("key_reminder_time", time)
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                }
            }
        }
    }
}