package com.rezaalamsyah.footballmatchschedule.view.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.LocalModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_list_favteam.view.*

class AdapterListFavTeam(private val context: FragmentActivity?,
                      private val listTeam: List<LocalModel.FavTeam>,
                      private val listener: (LocalModel.FavTeam) -> Unit):
        RecyclerView.Adapter<AdapterListFavTeam.MatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            MatchViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_list_favteam, parent, false))

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(listTeam[position],listener)
    }

    override fun getItemCount(): Int {
        return listTeam.size
    }

    class MatchViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        fun bindItem(items: LocalModel.FavTeam, listener: (LocalModel.FavTeam) -> Unit ) {
            Picasso.get().load(items.strBadge).into(itemView.img_favteam)
            itemView.txt_favteam.text = items.strTeam

            itemView.setOnClickListener {
                listener(items)
            }
        }
    }

}