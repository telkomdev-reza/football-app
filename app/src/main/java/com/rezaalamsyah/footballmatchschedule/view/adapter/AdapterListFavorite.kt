package com.rezaalamsyah.footballmatchschedule.view.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rezaalamsyah.footballmatchschedule.R.layout.*
import com.rezaalamsyah.footballmatchschedule.presenter.util.dayConversion
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.LocalModel
import kotlinx.android.synthetic.main.adapter_list_fav.view.*

class AdapterListFavorite(private val context: FragmentActivity?,
                          private val favMatch: List<LocalModel.Favorite>,
                          private val listener: (LocalModel.Favorite) -> Unit):
        RecyclerView.Adapter<AdapterListFavorite.MatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            MatchViewHolder(LayoutInflater.from(context).inflate(adapter_list_fav, parent, false))

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(favMatch[position],listener)
    }

    override fun getItemCount(): Int {
        return favMatch.size
    }

    class MatchViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        fun bindItem(items: LocalModel.Favorite, listener: (LocalModel.Favorite ) -> Unit ) {
            itemView.txt_fav_home_team.text = items.homeTeam
            itemView.txt_fav_away_team.text= items.awayTeam
            itemView.txt_score_home_fav.text = items.homeScore
            itemView.txt_score_away_fav.text = items.awayScore

            val matchDateIndo = dayConversion(items.dateEvent)
            itemView.txt_fav_datematch.text = matchDateIndo

            itemView.setOnClickListener {
                listener(items)
            }
        }
    }

}