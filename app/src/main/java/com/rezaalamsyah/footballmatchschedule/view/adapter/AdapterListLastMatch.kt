package com.rezaalamsyah.footballmatchschedule.view.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rezaalamsyah.footballmatchschedule.R.layout.adapter_list_match
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.presenter.util.convertTime
import com.rezaalamsyah.footballmatchschedule.presenter.util.dayConversion
import kotlinx.android.synthetic.main.adapter_list_match.view.*

class AdapterListLastMatch(private val context: FragmentActivity?,
                           private val lastMatch: List<LastMatch>,
                           private val listener: (LastMatch) -> Unit):
        RecyclerView.Adapter<AdapterListLastMatch.MatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            MatchViewHolder(LayoutInflater.from(context).inflate(adapter_list_match, parent, false))

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(lastMatch[position],listener)
    }

    override fun getItemCount(): Int {
        return lastMatch.size
    }

    class MatchViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        fun bindItem(items: LastMatch, listener: (LastMatch) -> Unit ) {
            itemView.txt_prev_home_team.text = items.strHomeTeam
            itemView.txt_prev_away_team.text= items.strAwayTeam
            itemView.txt_score_home.text = items.intHomeScore
            itemView.txt_score_away.text = items.intAwayScore
            val matchDateIndo = dayConversion(items.strDate)
            itemView.txt_prev_datematch.text = matchDateIndo
            val time = convertTime(items.strTime)
            itemView.txt_prev_time.text = time

            itemView.setOnClickListener {
                listener(items)
            }
        }
    }
}