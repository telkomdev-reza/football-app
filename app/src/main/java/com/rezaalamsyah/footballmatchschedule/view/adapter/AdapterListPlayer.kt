package com.rezaalamsyah.footballmatchschedule.view.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.model.content.Player
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_player.view.*

class AdapterListPlayer(private val context: FragmentActivity?,
                      private val listTeam: List<Player>,
                      private val listener: (Player) -> Unit):
        RecyclerView.Adapter<AdapterListPlayer.MatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            MatchViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_player, parent, false))

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(listTeam[position],listener)
    }

    override fun getItemCount(): Int {
        return listTeam.size
    }

    class MatchViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        fun bindItem(items: Player, listener: (Player) -> Unit ) {
            itemView.playerName.text = items.strPlayer
            itemView.playerPos.text = items.strPosition
            Picasso.get().load(items.strCutout).placeholder(R.drawable.ic_player).into(itemView.photoPlayer)

            itemView.setOnClickListener {
                listener(items)
            }
        }
    }

}