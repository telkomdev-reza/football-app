package com.rezaalamsyah.footballmatchschedule.view.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_list_team.view.*

class AdapterListTeam(private val context: FragmentActivity?,
                          private val listTeam: List<TeamDetails>,
                          private val listener: (TeamDetails) -> Unit):
        RecyclerView.Adapter<AdapterListTeam.MatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            MatchViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_list_team, parent, false))

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(listTeam[position],listener)
    }

    override fun getItemCount(): Int {
        return listTeam.size
    }

    class MatchViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        fun bindItem(items: TeamDetails, listener: (TeamDetails) -> Unit ) {
            itemView.txtTeam.text = items.strTeam
            Picasso.get().load(items.strTeamBadge).into(itemView.imgTeam)

            itemView.setOnClickListener {
                listener(items)
            }
        }
    }

}