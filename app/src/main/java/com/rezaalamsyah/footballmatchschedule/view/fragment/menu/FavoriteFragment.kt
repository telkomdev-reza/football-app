package com.rezaalamsyah.footballmatchschedule.view.fragment.menu

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.R.layout.fragment_fav
import com.rezaalamsyah.footballmatchschedule.view.adapter.PagerAdapter
import com.rezaalamsyah.footballmatchschedule.view.fragment.tab.FavMatchesFragment
import com.rezaalamsyah.footballmatchschedule.view.fragment.tab.FavteamsFragment

class FavoriteFragment : Fragment() {
    private lateinit var viewPager: ViewPager
    private lateinit var tabs: TabLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(fragment_fav, container, false)
        viewPager = view.findViewById(R.id.viewPagerFav)
        tabs = view.findViewById(R.id.tabsFav)
        setHasOptionsMenu(true)
        initPager()
        return view
    }

    private fun initPager(){
        val pageAdapter = PagerAdapter(childFragmentManager)
        pageAdapter.add(FavMatchesFragment.newInstance(),"Favorite Matches")
        pageAdapter.add(FavteamsFragment.newInstance(),"Favorite Teams")
        viewPager.adapter = pageAdapter
        tabs.setupWithViewPager(viewPager)
    }
}
