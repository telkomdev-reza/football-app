package com.rezaalamsyah.footballmatchschedule.view.fragment.menu

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.R.layout.fragment_matches
import com.rezaalamsyah.footballmatchschedule.view.adapter.PagerAdapter
import android.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx
import com.lapism.searchview.Search
import com.lapism.searchview.widget.SearchView
import com.rezaalamsyah.footballmatchschedule.view.activity.SearchActivity
import com.rezaalamsyah.footballmatchschedule.view.fragment.tab.NextMatchFragment
import com.rezaalamsyah.footballmatchschedule.view.fragment.tab.PrevMatchFragment


class MatchesFragment : Fragment() {
    private lateinit var viewPager: ViewPager
    private lateinit var tabs: TabLayout
    private lateinit var btnSearch : SearchView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val  view : View = inflater.inflate(fragment_matches, container, false)
        setHasOptionsMenu(true)
        viewPager = view.findViewById(R.id.viewPagerMatches)
        tabs = view.findViewById(R.id.tabsMatches)
        btnSearch = view.findViewById(R.id.searchViewMatches)
        initPager()
        searchAction()
        return view
    }

    override fun onResume() {
        super.onResume()
        btnSearch.setText(null)
        btnSearch.clearFocus()
    }

    private fun searchAction(){
        btnSearch.setOnQueryTextListener(object : Search.OnQueryTextListener {
            override fun onQueryTextSubmit(query: CharSequence): Boolean {
                ctx.startActivity<SearchActivity>(
                        "key_query" to query
                )
                return true
            }

            override fun onQueryTextChange(newText: CharSequence) {

            }
        })
    }

    private fun initPager(){
        val pageAdapter = PagerAdapter(childFragmentManager)
        pageAdapter.add(NextMatchFragment.newInstance(),"Next Matches")
        pageAdapter.add(PrevMatchFragment.newInstance(),"Past Matches")
        viewPager.adapter = pageAdapter
        tabs.setupWithViewPager(viewPager)
    }
}
