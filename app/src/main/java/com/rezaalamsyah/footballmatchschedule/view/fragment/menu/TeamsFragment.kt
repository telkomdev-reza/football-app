package com.rezaalamsyah.footballmatchschedule.view.fragment.menu

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import com.lapism.searchview.Search
import com.lapism.searchview.widget.SearchView
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.R.layout.fragment_teams
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.pres.TeamFragmentPres
import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.util.invisible
import com.rezaalamsyah.footballmatchschedule.presenter.util.visible
import com.rezaalamsyah.footballmatchschedule.presenter.view.TeamFragmentView
import com.rezaalamsyah.footballmatchschedule.view.activity.TeamDetailActivity
import com.rezaalamsyah.footballmatchschedule.view.adapter.AdapterListTeam
import kotlinx.android.synthetic.main.fragment_teams.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh

class TeamsFragment : Fragment(), TeamFragmentView {

    private lateinit var presenter : TeamFragmentPres
    private lateinit var adapter : AdapterListTeam
    private lateinit var swipeRef: SwipeRefreshLayout
    private lateinit var lvProgressBar: LinearLayout
    private lateinit var spLeague: Spinner
    private lateinit var search : SearchView
    private lateinit var rvSpinner : RelativeLayout
    private lateinit var line : View
    private var teams: MutableList<TeamDetails> = mutableListOf()
    private var idLeague = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(fragment_teams, container, false)
        lvProgressBar = view.findViewById(R.id.lvProgressBarTeam)
        swipeRef = view.findViewById(R.id.swipeRefreshTeam)
        spLeague= view.findViewById(R.id.spTeam)
        search = view.findViewById(R.id.searchViewTeams)
        rvSpinner = view.findViewById(R.id.rvSpinner)
        line = view.findViewById(R.id.rvLine)
        intentAction()
        view.rvTeam.layoutManager = LinearLayoutManager(context)
        view.rvTeam.adapter = adapter
        initRequest()
        searchAction()
        chooseLeague()
        refreshAction()
        return view
    }

    private fun initRequest(){
        val request = ApiRepo()
        val gson = Gson()
        val contextCoroutine = CoroutineContextProvider()
        presenter = TeamFragmentPres(this,request,gson,contextCoroutine)
    }

    private fun searchAction(){
        search.setOnQueryTextListener(object : Search.OnQueryTextListener {
            override fun onQueryTextSubmit(query: CharSequence): Boolean {

                return true
            }

            override fun onQueryTextChange(newText: CharSequence) {
                if(newText.length - 0 == 0){
                    teams.clear()
                    adapter.notifyDataSetChanged()
                    rvSpinner.visibility  = View.VISIBLE
                    line.visibility = View.VISIBLE
                    chooseLeague()
                }else{
                    rvSpinner.visibility  = View.GONE
                    line.visibility = View.GONE
                    presenter.getSearchTeam(newText.toString())
                }
            }
        })
    }

    private fun spinnerAction(){
        spLeague.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                idLeague = spLeague.selectedItem.toString()
                presenter.getListTeam(idLeague)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun chooseLeague(){
        val spinnerItems = resources.getStringArray(R.array.country)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spLeague.adapter = spinnerAdapter
        spinnerAction()
    }

    private fun intentAction(){
        val activity = activity
        adapter = AdapterListTeam(activity,teams){
            ctx.startActivity<TeamDetailActivity>(
                    "key_data" to it,
                    "key_idTeam" to it.idTeam,
                    "key_from" to "team"
            )
        }
    }

    override fun showLoading() {
        swipeRef.isRefreshing = false
        lvProgressBar.visible()
    }

    override fun hideLoading() {
        lvProgressBar.invisible()
    }

    override fun showTeamList(dataListTeam: List<TeamDetails>) {
        teams.clear()
        teams.addAll(dataListTeam)
        adapter.notifyDataSetChanged()
    }

    override fun nullData() {
        teams.clear()
        adapter.notifyDataSetChanged()
    }

    private fun refreshAction() {
        swipeRef.onRefresh {
            presenter.getListTeam(idLeague)
        }
    }
}
