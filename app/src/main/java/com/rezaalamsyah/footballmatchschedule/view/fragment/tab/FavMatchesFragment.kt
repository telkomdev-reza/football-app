package com.rezaalamsyah.footballmatchschedule.view.fragment.tab

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.R.layout.fragment_favorite
import com.rezaalamsyah.footballmatchschedule.presenter.util.invisible
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.LocalModel
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.database
import com.rezaalamsyah.footballmatchschedule.presenter.util.visible
import com.rezaalamsyah.footballmatchschedule.view.activity.MatchDetailActivity
import com.rezaalamsyah.footballmatchschedule.view.adapter.AdapterListFavorite
import kotlinx.android.synthetic.main.fragment_favorite.view.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh

class FavMatchesFragment : Fragment() {

    private lateinit var adapter : AdapterListFavorite
    private lateinit var swipeRef: SwipeRefreshLayout
    private lateinit var lvProgressBar: LinearLayout
    private var fav: MutableList<LocalModel.Favorite> = mutableListOf()
    private var loadingCode :String = ""

    companion object {
        fun newInstance(): FavMatchesFragment {
            val fragment = FavMatchesFragment()
            val args = Bundle()
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val  view : View = inflater.inflate(fragment_favorite, container, false)
        lvProgressBar = view.findViewById(R.id.lvProgressBarFav)
        swipeRef = view.findViewById(R.id.swipeRefreshFav)
        intentAction()
        view.rvFavMatch.layoutManager = LinearLayoutManager(ctx)
        view.rvFavMatch.adapter = adapter
        showFavorite()
        refreshAction()
        return view
    }

    private fun showFavorite(){
        context?.database?.use {
            if (loadingCode.equals("load")) lvProgressBar.invisible()
            swipeRef.isRefreshing = false
            val result = select(LocalModel.Favorite.TABLE_FAV)
            val favorite = result.parseList(classParser<LocalModel.Favorite>())
            fav.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }

    private fun intentAction(){
        val activity = activity
        adapter = AdapterListFavorite(activity,fav){
            ctx.startActivity<MatchDetailActivity>(
                    "key_data" to it,
                    "key_idEvent" to it.teamId,
                    "key_homeTeam" to it.homeTeam,
                    "key_awayTeam" to it.awayTeam,
                    "key_from" to "favorite"
            )
        }
    }

    private fun refreshAction() {
        swipeRef.onRefresh {
            loadingCode = "load"
            lvProgressBar.visible()
            fav.clear()
            showFavorite()
        }
    }
}
