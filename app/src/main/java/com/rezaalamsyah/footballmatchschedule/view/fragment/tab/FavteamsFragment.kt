package com.rezaalamsyah.footballmatchschedule.view.fragment.tab

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.R.layout.fragment_favteams
import com.rezaalamsyah.footballmatchschedule.presenter.util.invisible
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.LocalModel
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.database
import com.rezaalamsyah.footballmatchschedule.presenter.util.visible
import com.rezaalamsyah.footballmatchschedule.view.activity.TeamDetailActivity
import com.rezaalamsyah.footballmatchschedule.view.adapter.AdapterListFavTeam
import kotlinx.android.synthetic.main.fragment_favteams.view.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh

class FavteamsFragment : Fragment() {

    private lateinit var adapter : AdapterListFavTeam
    private lateinit var swipeRef: SwipeRefreshLayout
    private lateinit var lvProgressBar: LinearLayout
    private var favteams: MutableList<LocalModel.FavTeam> = mutableListOf()
    private var loadingCode :String = ""

    companion object {
        fun newInstance(): FavteamsFragment {
            val fragment = FavteamsFragment()
            val args = Bundle()
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view : View = inflater.inflate(fragment_favteams, container, false)
        lvProgressBar = view.findViewById(R.id.lvProgressBarFavteam)
        swipeRef = view.findViewById(R.id.swipeRefreshFavteam)
        intentAction()
        view.rvFavteam.layoutManager = GridLayoutManager(ctx,2)
        view.rvFavteam.adapter = adapter
        showFavorite()
        refreshAction()
        return view
    }

    private fun showFavorite(){
        context?.database?.use {
            if (loadingCode.equals("load")) lvProgressBar.invisible()
            swipeRef.isRefreshing = false
            val result = select(LocalModel.FavTeam.TABLE_FAVTEAM)
            val favorite = result.parseList(classParser<LocalModel.FavTeam>())
            favteams.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }

    private fun intentAction(){
        val activity = activity
        adapter = AdapterListFavTeam(activity,favteams){
            ctx.startActivity<TeamDetailActivity>(
                    "key_data" to it,
                    "key_idTeam" to it.teamId,
                    "key_from" to "favorite"
            )
        }
    }

    private fun refreshAction() {
        swipeRef.onRefresh {
            loadingCode = "load"
            lvProgressBar.visible()
            favteams.clear()
            showFavorite()
        }
    }
}
