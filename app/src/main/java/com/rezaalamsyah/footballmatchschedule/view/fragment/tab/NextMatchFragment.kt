package com.rezaalamsyah.footballmatchschedule.view.fragment.tab

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner
import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.R.array.country
import com.rezaalamsyah.footballmatchschedule.R.layout.fragment_next_match
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.pres.NextMatchFragmentPres
import com.rezaalamsyah.footballmatchschedule.presenter.view.ActivityMainView
import com.rezaalamsyah.footballmatchschedule.view.activity.MatchDetailActivity
import com.rezaalamsyah.footballmatchschedule.view.adapter.AdapteListNextMatch
import kotlinx.android.synthetic.main.fragment_next_match.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import android.provider.CalendarContract
import com.rezaalamsyah.footballmatchschedule.presenter.util.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class NextMatchFragment : Fragment(),ActivityMainView {

    private lateinit var presenter : NextMatchFragmentPres
    private lateinit var adapter : AdapteListNextMatch
    private lateinit var swipeRef: SwipeRefreshLayout
    private var match: MutableList<LastMatch> = mutableListOf()
    private lateinit var lvProgressBar: LinearLayout
    private lateinit var spLeague: Spinner
    private var idLeague = ""
    private var matchDate = ""
    private var matchTitle = ""
    private var matchDesc = ""
    private var matchTime = ""

    companion object {
        fun newInstance(): NextMatchFragment {
            val fragment = NextMatchFragment()
            val args = Bundle()
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val  view : View = inflater.inflate(fragment_next_match, container, false)
        lvProgressBar= view.findViewById(R.id.lvProgressBarNext)
        swipeRef = view.findViewById(R.id.swipeRefreshNext)
        spLeague= view.findViewById(R.id.spLeagueNext)
        intentAction()
        view.rvNextMatch.layoutManager = LinearLayoutManager(context)
        view.rvNextMatch.adapter = adapter
        chooseLeague()
        refreshAction()
        LocalBroadcastManager.getInstance(ctx).
                registerReceiver(mReceiver, IntentFilter("reminder"))

        return view
    }

    private fun chooseLeague(){
        val spinnerItems = resources.getStringArray(country)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spLeague.adapter = spinnerAdapter

        val request = ApiRepo()
        val gson = Gson()
        val contextCoroutine = CoroutineContextProvider()
        presenter = NextMatchFragmentPres(this,request,gson,contextCoroutine)

        spLeague.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                idLeague = getIdLeague(spLeague.selectedItem.toString())
                presenter.getNextMatch(idLeague)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            matchDate = intent.getStringExtra("key_reminder_date")
            matchTitle = intent.getStringExtra("key_reminder_title")
            matchDesc = intent.getStringExtra("key_reminder_desc")
            matchTime = intent.getStringExtra("key_reminder_time")
            addReminder(matchDate,matchTitle,matchDesc,matchTime)
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun addReminder(date : String, title: String, desc: String, time: String) {

        val intent = Intent(Intent.ACTION_INSERT)
        val datetime = date + " " + time
        val dateObject: Date
        val dateObject2: Date
        val lnsTime: Long
        val lneTime: Long

        intent.type = "vnd.android.cursor.item/event"
        val formatter: DateFormat = SimpleDateFormat("d/M/yy h:mm")
        val endTime = formatter.parse(datetime)
        val d: Long = endTime.time
        val dmin: Long
        dmin = d - (2 * 60 * 60 * 1000)
        val startTime = Date(dmin)

        val df: DateFormat = SimpleDateFormat("d/M/yy h:mm")
        val dfa: DateFormat = SimpleDateFormat("E MMM dd hh:mm:ss z yyyy")
        dateObject = df.parse(datetime)
        lnsTime = dateObject.getTime()
        dateObject2 = dfa.parse(startTime.toString())
        lneTime = dateObject2.getTime()
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, lneTime)
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, lnsTime)
        intent.putExtra(CalendarContract.Events.CALENDAR_ID, 1)
        intent.putExtra(CalendarContract.Events.TITLE, title)
        intent.putExtra(CalendarContract.Events.DESCRIPTION, desc)
        intent.putExtra(CalendarContract.Events.HAS_ALARM, 1)

        ctx.startActivity(intent)
    }

    private fun intentAction(){
        adapter = AdapteListNextMatch(activity,ctx,match){
            ctx.startActivity<MatchDetailActivity>(
                    "key_data" to it,
                    "key_idEvent" to it.idEvent,
                    "key_homeTeam" to it.strHomeTeam,
                    "key_awayTeam" to it.strAwayTeam,
                    "key_from" to "match"
            )
        }
    }

    override fun showLoading() {
        swipeRef.isRefreshing = false
        lvProgressBar.visible()
    }

    override fun hideLoading() {
        lvProgressBar.invisible()
    }

    override fun showMatchList(dataLastMatch: List<LastMatch>) {
        match.clear()
        match.addAll(dataLastMatch)
        adapter.notifyDataSetChanged()
    }

    override fun nullData() {
        match.clear()
        adapter.notifyDataSetChanged()
    }

    private fun refreshAction() {
        swipeRef.onRefresh {
            presenter.getNextMatch(idLeague)
        }
    }
}
