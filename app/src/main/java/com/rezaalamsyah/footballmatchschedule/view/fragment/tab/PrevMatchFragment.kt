package com.rezaalamsyah.footballmatchschedule.view.fragment.tab

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner
import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.R.layout.fragment_prev_match
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.pres.PrevMatchFragmentPres
import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.util.getIdLeague
import com.rezaalamsyah.footballmatchschedule.presenter.util.invisible
import com.rezaalamsyah.footballmatchschedule.presenter.util.visible
import com.rezaalamsyah.footballmatchschedule.presenter.view.ActivityMainView
import com.rezaalamsyah.footballmatchschedule.view.activity.MatchDetailActivity
import com.rezaalamsyah.footballmatchschedule.view.adapter.AdapterListLastMatch
import kotlinx.android.synthetic.main.fragment_prev_match.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh

class PrevMatchFragment : Fragment(), ActivityMainView {

    private lateinit var presenter : PrevMatchFragmentPres
    private lateinit var adapter : AdapterListLastMatch
    private lateinit var swipeRef: SwipeRefreshLayout
    private var match: MutableList<LastMatch> = mutableListOf()
    private lateinit var lvProgressBar: LinearLayout
    private lateinit var spLeague: Spinner
    private var idLeague = ""

    companion object {
        fun newInstance(): PrevMatchFragment {
            val fragment = PrevMatchFragment()
            val args = Bundle()
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val  view : View = inflater.inflate(fragment_prev_match, container, false)
        lvProgressBar = view.findViewById(R.id.lvProgressBar)
        swipeRef = view.findViewById(R.id.swipeRefresh)
        spLeague= view.findViewById(R.id.spLeaguePrev)
        intentAction()
        view.rvLastMatch.layoutManager = LinearLayoutManager(context)
        view.rvLastMatch.adapter = adapter
        chooseLeague()
        refreshAction()
        return view
    }

    private fun chooseLeague(){
        val spinnerItems = resources.getStringArray(R.array.country)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spLeague.adapter = spinnerAdapter

        val request = ApiRepo()
        val gson = Gson()
        val contextCoroutine = CoroutineContextProvider()
        presenter = PrevMatchFragmentPres(this,request,gson,contextCoroutine)

        spLeague.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                idLeague = getIdLeague(spLeague.selectedItem.toString())
                presenter.getLastMatch(idLeague)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun intentAction(){
        val activity = activity
        adapter = AdapterListLastMatch(activity,match){
            ctx.startActivity<MatchDetailActivity>(
                    "key_data" to it,
                    "key_idEvent" to it.idEvent,
                    "key_homeTeam" to it.strHomeTeam,
                    "key_awayTeam" to it.strAwayTeam,
                    "key_from" to "match"
            )
        }
    }

    override fun showLoading() {
        swipeRef.isRefreshing = false
        lvProgressBar.visible()
    }

    override fun hideLoading() {
        lvProgressBar.invisible()
    }

    override fun showMatchList(dataLastMatch: List<LastMatch>) {
        match.clear()
        match.addAll(dataLastMatch)
        adapter.notifyDataSetChanged()
    }
    override fun nullData() {
        match.clear()
        adapter.notifyDataSetChanged()
    }

    private fun refreshAction() {
        swipeRef.onRefresh {
            presenter.getLastMatch(idLeague)
        }
    }
}
