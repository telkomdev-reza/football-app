package com.rezaalamsyah.footballmatchschedule.view.fragment.tab

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rezaalamsyah.footballmatchschedule.R.layout.fragment_team_overview
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.LocalModel
import kotlinx.android.synthetic.main.fragment_team_overview.view.*

class TeamOverviewFragment : Fragment() {

    companion object {
        fun newInstance(data : TeamDetails?, dataFav : LocalModel.FavTeam?): TeamOverviewFragment {
            val fragment = TeamOverviewFragment()
            val args = Bundle()
            if (data != null)
                args.putString("key_desc",data.strDescriptionEN)
            else{
                if (dataFav != null)
                    args.putString("key_desc",dataFav.strDesc)}
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(fragment_team_overview, container, false)
        val desc = arguments!!.getString("key_desc")
        view.teamDesc.text = desc
        return view
    }
}
