package com.rezaalamsyah.footballmatchschedule.view.fragment.tab

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.R
import com.rezaalamsyah.footballmatchschedule.R.layout.fragment_team_player
import com.rezaalamsyah.footballmatchschedule.model.content.Player
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.pres.TeamPlayerPres
import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.util.invisible
import com.rezaalamsyah.footballmatchschedule.presenter.util.visible
import com.rezaalamsyah.footballmatchschedule.presenter.view.TeamPlayerView
import com.rezaalamsyah.footballmatchschedule.view.activity.PlayerDetailActivity
import com.rezaalamsyah.footballmatchschedule.view.adapter.AdapterListPlayer
import kotlinx.android.synthetic.main.fragment_team_player.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh

class TeamPlayerFragment : Fragment(), TeamPlayerView {

    private lateinit var presenter : TeamPlayerPres
    private lateinit var adapter : AdapterListPlayer
    private lateinit var swipeRef: SwipeRefreshLayout
    private lateinit var lvProgressBar: LinearLayout
    private var players: MutableList<Player> = mutableListOf()
    private var idTeam = ""

    companion object {
        fun newInstance(data : String): TeamPlayerFragment {
            val fragment = TeamPlayerFragment()
            val args = Bundle()
            args.putString("key_idTeam",data)
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(fragment_team_player, container, false)
        lvProgressBar = view.findViewById(R.id.lvProgressBarPlayer)
        swipeRef = view.findViewById(R.id.swipeRefreshPlayer)
        intentAction()
        view.rvPlayer.layoutManager = GridLayoutManager(context, 2)
        view.rvPlayer.adapter = adapter
        initRequest()
        refreshAction()
        return view
    }

    private fun initRequest(){
        val request = ApiRepo()
        val gson = Gson()
        val contextCoroutine = CoroutineContextProvider()
        idTeam = arguments!!.getString("key_idTeam")
        presenter = TeamPlayerPres(this,request,gson,contextCoroutine)
        presenter.getListPlayer(idTeam)
    }

    private fun intentAction(){
        val activity = activity
        adapter = AdapterListPlayer(activity,players){
            ctx.startActivity<PlayerDetailActivity>(
                    "key_data" to it
            )
        }
    }

    override fun showLoading() {
        swipeRef.isRefreshing = false
        lvProgressBar.visible()
    }

    override fun hideLoading() {
        lvProgressBar.invisible()
    }

    override fun showTeamPlayer(dataPlayer: List<Player>) {
        println("goblo "+dataPlayer)
        players.clear()
        players.addAll(dataPlayer)
        adapter.notifyDataSetChanged()
    }

    override fun nullData() {
        players.clear()
        adapter.notifyDataSetChanged()
    }

    private fun refreshAction() {
        swipeRef.onRefresh {
            presenter.getListPlayer(idTeam)
        }
    }
}
