package com.rezaalamsyah.footballmatchschedule.view.viewholder

import android.app.Activity
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails
import com.rezaalamsyah.footballmatchschedule.presenter.util.*
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.LocalModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_match_detail.*

fun vwMatchDetails(dataMatchDetails: LastMatch, view : Activity){
    view.colalapsing_detailprev.title = dataMatchDetails.strEvent
    view.detailprev_strEvent.text = dataMatchDetails.strEvent
    view.detailprev_scoreHome.text = dataMatchDetails.intHomeScore
    view.detailprev_scoreAway.text = dataMatchDetails.intAwayScore
    view.detailprev_lineupShotHome.text = dataMatchDetails.intHomeShots
    view.detailprev_lineupShotAway.text = dataMatchDetails.intAwayShots
    view.detailprev_formHome.text = replaceString(dataMatchDetails.strHomeFormation)
    view.detailprev_formAway.text = replaceString(dataMatchDetails.strAwayFormation)
    view.detailprev_lineupGoalHome.text = replaceStrColNospace(dataMatchDetails.strHomeGoalDetails)
    view.detailprev_lineupGoalAway.text = replaceStrColNospace(dataMatchDetails.strAwayGoalDetails)
    view.detailprev_lineupRedHome.text = replaceString(dataMatchDetails.strHomeRedCards)
    view.detailprev_lineupRedAway.text = replaceString(dataMatchDetails.strHomeRedCards)
    view.detailprev_lineupYellowHome.text = replaceStrColNospace(dataMatchDetails.strHomeYellowCards)
    view.detailprev_lineupYellowAway.text = replaceStrColNospace(dataMatchDetails.strAwayYellowCards)
    view.detailprev_lineupGKHome.text = replaceString(dataMatchDetails.strHomeLineupGoalkeeper)
    view.detailprev_lineupGKAway.text = replaceString(dataMatchDetails.strAwayLineupGoalkeeper)
    view.detailprev_lineupBackHome.text = replaceString(dataMatchDetails.strHomeLineupDefense)
    view.detailprev_lineupBackAway.text = replaceString(dataMatchDetails.strAwayLineupDefense)
    view.detailprev_lineupMidHome.text = replaceString(dataMatchDetails.strHomeLineupMidfield)
    view.detailprev_lineupMidAway.text = replaceString(dataMatchDetails.strAwayLineupMidfield)
    view.detailprev_lineupFwdHome.text = replaceString(dataMatchDetails.strHomeLineupForward)
    view.detailprev_lineupFwdAway.text = replaceString(dataMatchDetails.strAwayLineupForward)
    view.detailprev_date.text = dayConversion(dataMatchDetails.strDate)
}

fun vwFavMatchDetails(dataMatchDetails: LocalModel.Favorite, view : Activity){
    view.colalapsing_detailprev.title = dataMatchDetails.eventName
    view.detailprev_strEvent.text = dataMatchDetails.eventName
    view.detailprev_scoreHome.text = dataMatchDetails.homeScore
    view.detailprev_scoreAway.text = dataMatchDetails.awayScore
    view.detailprev_lineupShotHome.text = dataMatchDetails.homeShot
    view.detailprev_lineupShotAway.text = dataMatchDetails.awayShot
    view.detailprev_formHome.text = replaceString(dataMatchDetails.homeForm)
    view.detailprev_formAway.text = replaceString(dataMatchDetails.awayForm)
    view.detailprev_lineupGoalHome.text = replaceStrColNospace(dataMatchDetails.homeGoald)
    view.detailprev_lineupGoalAway.text = replaceStrColNospace(dataMatchDetails.awayGoald)
    view.detailprev_lineupRedHome.text = replaceString(dataMatchDetails.homeRcard)
    view.detailprev_lineupRedAway.text = replaceString(dataMatchDetails.awayRcard)
    view.detailprev_lineupYellowHome.text = replaceStrColNospace(dataMatchDetails.homeYcard)
    view.detailprev_lineupYellowAway.text = replaceStrColNospace(dataMatchDetails.awayYcard)
    view.detailprev_lineupGKHome.text = replaceString(dataMatchDetails.homeGk)
    view.detailprev_lineupGKAway.text = replaceString(dataMatchDetails.awayGK)
    view.detailprev_lineupBackHome.text = replaceString(dataMatchDetails.homeDef)
    view.detailprev_lineupBackAway.text = replaceString(dataMatchDetails.awayDef)
    view.detailprev_lineupMidHome.text = replaceString(dataMatchDetails.homeMid)
    view.detailprev_lineupMidAway.text = replaceString(dataMatchDetails.awayMid)
    view.detailprev_lineupFwdHome.text = replaceString(dataMatchDetails.homeFwd)
    view.detailprev_lineupFwdAway.text = replaceString(dataMatchDetails.awayFwd)
    view.detailprev_date.text = dayConversion(dataMatchDetails.dateEvent)
}

fun vwHomeTeamDetails(dataTeamDetails: List<TeamDetails>, view : Activity){
    Picasso.get().load(dataTeamDetails.get(0).strTeamBadge).into(view.detailprev_imgHome)
}

fun vwAwayTeamDetails(dataTeamDetails: List<TeamDetails>, view : Activity){
    Picasso.get().load(dataTeamDetails.get(0).strTeamBadge).into(view.detailprev_imgAway)
}