package com.rezaalamsyah.footballmatchschedule.view.viewholder

import android.app.Activity
import com.rezaalamsyah.footballmatchschedule.model.content.Player
import com.rezaalamsyah.footballmatchschedule.presenter.util.replaceString
import kotlinx.android.synthetic.main.activity_player_detail.*

fun vwPlayerDetail(data: Player, view: Activity){
    view.playerTeam.text = replaceString(data.strTeam)
    view.playerNational.text = replaceString(data.strNationality)
    view.colalapsing_detailplayer.title = replaceString(data.strPlayer)
    view.playerPosition.text = replaceString(data.strPosition)
    view.txtWeight.text = replaceString(data.strWeight)
    view.txtHeight.text = replaceString(data.strHeight)
    view.txtFb.text = replaceString(data.strFacebook)
    view.txtTwitter.text = replaceString(data.strTwitter)
    view.txtInsta.text = replaceString(data.strInstagram)
    view.playerDesc.text = replaceString(data.strDescriptionEN)
}