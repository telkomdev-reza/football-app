package com.rezaalamsyah.footballmatchschedule.view.viewholder

import android.app.Activity
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails
import com.rezaalamsyah.footballmatchschedule.presenter.util.localstorage.LocalModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_team_detail.*

fun vwTeamDetail(data : TeamDetails?, view : Activity){
    if (data != null) {
        view.colalapsing_detailteam.title = data.strTeam
        view.teamFormedYear.text = data.intFormedYear
        view.teamStadium.text = data.strStadium
        Picasso.get().load(data.strTeamBadge).into(view.imgDetailteam)
    }
}

fun vwFavTeamDetail(data : LocalModel.FavTeam?, view : Activity){
    if (data != null) {
        view.colalapsing_detailteam.title = data.strTeam
        view.teamFormedYear.text = data.strYear
        view.teamStadium.text = data.strStadium
        Picasso.get().load(data.strBadge).into(view.imgDetailteam)
    }
}