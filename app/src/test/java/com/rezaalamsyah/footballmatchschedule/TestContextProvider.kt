package com.rezaalamsyah.footballmatchschedule

import com.rezaalamsyah.footballmatchschedule.presenter.util.CoroutineContextProvider
import kotlinx.coroutines.experimental.Unconfined
import kotlin.coroutines.experimental.CoroutineContext

class TestContextProvider : CoroutineContextProvider() {
    override val main: CoroutineContext = Unconfined
}