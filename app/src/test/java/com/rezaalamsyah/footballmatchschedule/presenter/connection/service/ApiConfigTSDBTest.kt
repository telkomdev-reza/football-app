package com.rezaalamsyah.footballmatchschedule.presenter.connection.service

import org.junit.Test

import org.junit.Assert.*

class ApiConfigTSDBTest {

    @Test
    fun getLastMatch() {
        val leagueId ="4328"
        assertEquals("https://www.thesportsdb.com/api/v1/json/1/eventspastleague.php?id=4328",ApiConfigTSDB.getLastMatch(leagueId))
    }

    @Test
    fun getNextMatch() {
        val leagueId ="4328"
        assertEquals("https://www.thesportsdb.com/api/v1/json/1/eventsnextleague.php?id=4328",ApiConfigTSDB.getNextMatch(leagueId))
    }

    @Test
    fun getTeamDetail() {
        val team="Leicester"
        assertEquals("https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=Leicester",ApiConfigTSDB.getTeamDetail(team))
    }

    @Test
    fun getLisTeam() {
        val leagueId ="English Premier League"
        assertEquals("https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=English Premier League",ApiConfigTSDB.getListTeam(leagueId))
    }

    @Test
    fun getListPlayer() {
        val leagueId ="133604"
        assertEquals("https://www.thesportsdb.com/api/v1/json/1/lookup_all_players.php?id=133604",ApiConfigTSDB.getListPlayer(leagueId))
    }

    @Test
    fun getSearchMatch() {
        val team="Arsenal"
        assertEquals("https://www.thesportsdb.com/api/v1/json/1/searchevents.php?e=Arsenal",ApiConfigTSDB.getSearchMatch(team))
    }

}