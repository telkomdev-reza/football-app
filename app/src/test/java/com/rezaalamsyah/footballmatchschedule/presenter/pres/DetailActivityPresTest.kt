package com.rezaalamsyah.footballmatchschedule.presenter.pres

import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.TestContextProvider
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails
import com.rezaalamsyah.footballmatchschedule.model.response.Model
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiConfigTSDB
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.view.DetailActivityView
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class DetailActivityPresTest {

    @Mock
    private
    lateinit var view: DetailActivityView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepo

    private lateinit var presenter: DetailActivityPres

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = DetailActivityPres(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getTeamDetails() {
        val team: MutableList<TeamDetails> = mutableListOf()
        val response = Model.TeamDetail(team)
        val homeTeam = "Man United"
        val awayTeam = "Leicester"

        `when`(gson.fromJson(apiRepository
                .doRequest(ApiConfigTSDB.getTeamDetail(homeTeam)),
                Model.TeamDetail::class.java
        )).thenReturn(response)

        `when`(gson.fromJson(apiRepository
                .doRequest(ApiConfigTSDB.getTeamDetail(awayTeam)),
                Model.TeamDetail::class.java
        )).thenReturn(response)

        presenter.getTeamDetails(homeTeam,awayTeam)
        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).showHomeTeamDetails(team)
        verify(view).showAwayTeamDetails(team)
    }
}