package com.rezaalamsyah.footballmatchschedule.presenter.pres

import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.TestContextProvider
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.model.response.Model
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiConfigTSDB
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.view.ActivityMainView
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class NextMatchFragmentPresTest {

    @Mock
    private
    lateinit var view: ActivityMainView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepo

    private lateinit var presenter: NextMatchFragmentPres

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = NextMatchFragmentPres(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getNextMatch() {
        val match : MutableList<LastMatch> = mutableListOf()
        val response = Model.Events(match)
        val leagueId = "4328"

        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(ApiConfigTSDB.getNextMatch(leagueId)),
                Model.Events::class.java
        )).thenReturn(response)

        presenter.getNextMatch(leagueId)
        Mockito.verify(view).showLoading()
        Mockito.verify(view).hideLoading()
        Mockito.verify(view).showMatchList(match)
    }
}