package com.rezaalamsyah.footballmatchschedule.presenter.pres

import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.TestContextProvider
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.model.content.TeamDetails
import com.rezaalamsyah.footballmatchschedule.model.response.Model
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiConfigTSDB
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.view.ActivityMainView
import com.rezaalamsyah.footballmatchschedule.presenter.view.TeamFragmentView
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class TeamFragmentPresTest{

    @Mock
    private
    lateinit var view: TeamFragmentView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepo

    private lateinit var presenter: TeamFragmentPres

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = TeamFragmentPres(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getListTeam() {
        val match : MutableList<TeamDetails> = mutableListOf()
        val response = Model.TeamDetail(match)
        val leagueId = "English Premier League"

        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(ApiConfigTSDB.getListTeam(leagueId)),
                Model.TeamDetail::class.java
        )).thenReturn(response)

        presenter.getListTeam(leagueId)
        Mockito.verify(view).showLoading()
        Mockito.verify(view).hideLoading()
        Mockito.verify(view).showTeamList(match)
    }

}