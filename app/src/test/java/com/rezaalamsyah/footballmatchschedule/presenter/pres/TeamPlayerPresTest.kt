package com.rezaalamsyah.footballmatchschedule.presenter.pres

import com.google.gson.Gson
import com.rezaalamsyah.footballmatchschedule.TestContextProvider
import com.rezaalamsyah.footballmatchschedule.model.content.LastMatch
import com.rezaalamsyah.footballmatchschedule.model.content.Player
import com.rezaalamsyah.footballmatchschedule.model.response.Model
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiConfigTSDB
import com.rezaalamsyah.footballmatchschedule.presenter.connection.service.ApiRepo
import com.rezaalamsyah.footballmatchschedule.presenter.view.ActivityMainView
import com.rezaalamsyah.footballmatchschedule.presenter.view.TeamPlayerView
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class TeamPlayerPresTest{

    @Mock
    private
    lateinit var view: TeamPlayerView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepo

    private lateinit var presenter: TeamPlayerPres

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = TeamPlayerPres(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getLastMatch() {
        val match : MutableList<Player> = mutableListOf()
        val response = Model.Players(match)
        val leagueId = "4328"

        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(ApiConfigTSDB.getListPlayer(leagueId)),
                Model.Players::class.java
        )).thenReturn(response)

        presenter.getListPlayer(leagueId)
        Mockito.verify(view).showLoading()
        Mockito.verify(view).hideLoading()
        Mockito.verify(view).showTeamPlayer(match)
    }
}