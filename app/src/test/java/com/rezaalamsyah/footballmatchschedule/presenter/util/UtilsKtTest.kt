package com.rezaalamsyah.footballmatchschedule.presenter.util

import android.graphics.Color
import com.rezaalamsyah.footballmatchschedule.TestContextProvider
import com.rezaalamsyah.footballmatchschedule.presenter.pres.DetailActivityPres
import com.rezaalamsyah.footballmatchschedule.presenter.view.DetailActivityView
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class UtilsKtTest {

    @Test
    fun dayConversion() {
        val strSun= "5/08/18"
        val strMon= "6/08/18"
        val strTue= "7/08/18"
        val strWed= "8/08/18"
        val strThu= "9/08/18"
        val strFri= "10/08/18"
        val strSat= "11/08/18"
        assertEquals("Min, 5 Aug 2018", dayConversion(strSun))
        assertEquals("Sen, 6 Aug 2018", dayConversion(strMon))
        assertEquals("Sel, 7 Aug 2018", dayConversion(strTue))
        assertEquals("Rab, 8 Aug 2018", dayConversion(strWed))
        assertEquals("Kam, 9 Aug 2018", dayConversion(strThu))
        assertEquals("Jum, 10 Aug 2018", dayConversion(strFri))
        assertEquals("Sab, 11 Aug 2018", dayConversion(strSat))
        assertEquals("To be confirmed soon", dayConversion(null))
    }

    @Test
    fun convertTime() {
        val str ="14:00:00+00:00"
        assertEquals("14:00", convertTime(str))
        assertEquals("-", convertTime(null))
        assertEquals("-", convertTime(""))
    }

    @Test
    fun replaceString() {
        val str ="Test1; Test2"
        assertEquals("Test1\nTest2", replaceString(str))
        assertEquals("-", replaceString(null))
        assertEquals("4-3-2-1", replaceString("4-3-2-1"))
        assertEquals("-", replaceString(""))
    }

    @Test
    fun replaceStrColNospace() {
        val str ="Test1;Test2"
        assertEquals("Test1\nTest2", replaceStrColNospace(str))
        assertEquals("-", replaceStrColNospace(null))
    }

    @Test
    fun getIdLeague() {
        val str1 ="English Premier League"
        val str2 ="Spanish La Liga"
        val str3 ="German Bundesliga"
        val str4 ="Italian Serie A"
        val str5 ="French Ligue 1"
        assertEquals("4328", getIdLeague(str1))
        assertEquals("4399", getIdLeague(str3))
        assertEquals("4335", getIdLeague(str2))
        assertEquals("4332", getIdLeague(str4))
        assertEquals("4334", getIdLeague(str5))
    }

}